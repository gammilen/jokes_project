from flask import g, jsonify, request
from flask import jsonify, make_response
from jokes import app
from werkzeug.http import HTTP_STATUS_CODES
from .models import User, History
from jokes import auth


def error_response(status_code, message=None):
	payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
	if message:
		payload['message'] = message
	response = jsonify(payload)
	response.status_code = status_code
	return response


@app.errorhandler(400)
def bad_request(error):
	return error_response(400)


@app.errorhandler(500)
def bad_request(error):
	return error_response(500)


@app.errorhandler(404)
def bad_request(error):
	return error_response(404)


@auth.verify_password
def verify_password(username, password):
	"""Проверяет, что пользователь с введенными данными существует"""
	user = User.query.filter_by(username=username).first()
	if user is None:
		return False
	g.current_user = user
	return user.check_password(password)


@auth.error_handler
def auth_error():
	return error_response(401)


import functools
def save_request(f):
	"""Сохраняет информацию об обращении
	Если пользователь авторизировался, сохраняет его идентификатор
	"""
	@functools.wraps(f)
	def wrapped(*args, **kwargs):
		result = f(*args, **kwargs)
		user = None
		if hasattr(g, 'current_user'):
			user = g.current_user
		h = History(user=user, ip_address=request.remote_addr, url=request.url)
		h.save()
		return result
	return wrapped


def get_potential_errors(joke):
	"""Проверяет полученный результат поиска шутки
	Существует ли и принадлежит ли текущему пользователю
	"""
	if not joke:
		return error_response(404, 'Joke does not exist')
	if not joke.user == g.current_user:
		return error_response(403, 'You have not got access to this joke')
	return None
