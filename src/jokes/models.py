from datetime import datetime
from jokes import db, ma
from marshmallow import fields, validates, ValidationError


class Base(db.Model):
	__abstract__ = True

	def delete(self):
		db.session.delete(self)
		db.session.commit()

	def save(self):
		db.session.add(self)
		db.session.commit()


class User(Base):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True, nullable=False)
	password = db.Column(db.String(128))
	
	def __repr__(self):
		return '<User {}>'.format(self.username)

	def check_password(self, password):
		return password == self.password


class Joke(Base):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	user = db.relationship('User', backref=db.backref('jokes', lazy=True), single_parent=True)
	text = db.Column(db.String(1024), unique=True)

	def __repr__(self):
		return '<Joke {}>'.format(self.text)
	

class History(Base):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	user = db.relationship('User', backref=db.backref('history', lazy=True))
	ip_address = db.Column(db.String(15))
	url = db.Column(db.String())
	timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)


class JokeSchema(ma.ModelSchema):
	id = fields.Integer(dump_only=True)
	text = fields.String(required=True, unique=True)
	class Meta:
		model = Joke
		sqla_session = db.session
		fields = ('id', 'text')

	@validates('text')
	def validate_text(self, data, **kwargs):
		if Joke.query.filter_by(text=data).first():
			raise ValidationError('Your joke is not unique')