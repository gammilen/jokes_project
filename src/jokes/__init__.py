from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import Config
from flask_marshmallow import Marshmallow
from flask_httpauth import HTTPBasicAuth


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
ma = Marshmallow(app)
auth = HTTPBasicAuth()

from jokes import routes, models
