# -*- coding: utf-8 -*-
import requests
from flask import render_template
from jokes import app, db, auth
from .models import User, Joke, JokeSchema, History
from flask import g, request, abort, make_response, jsonify
from .utils import *
from marshmallow import ValidationError


generate_joke_url = "https://geek-jokes.sameerkumar.website/api"
joke_schema = JokeSchema()
jokes_schema = JokeSchema(many=True)


@app.route('/jokes/generate', methods=['GET'])
@auth.login_required
@save_request
def generate_joke():
	"""Запрашивает шутку у стороннего ресурса и возвращает ее пользователю"""
	error = False
	try:
		joke_response = requests.get(generate_joke_url)
	except:
		error = True
	if error or joke_response.status_code != 200:
		return make_response(({'message': 
			"Sorry, we cannot generate a joke for you"}, 503))
	return jsonify({'text': joke_response.text})


@app.route('/jokes', methods=['GET'])
@auth.login_required
@save_request
def get_my_jokes():
	"""Возвращает шутки текущего пользователя"""
	jokes = g.current_user.jokes
	return jokes_schema.jsonify(jokes)


@app.route('/jokes/<int:id>', methods=['GET'])
@auth.login_required
@save_request
def get_my_joke(id):
	"""Возвращает информацию о выбранной шутке, 
	если она принадлежит текущему пользователю
	"""
	joke = Joke.query.filter_by(id=id).one_or_none()
	error = get_potential_errors(joke)
	if error:
		return error
	return joke_schema.jsonify(joke)


@app.route('/jokes', methods=['POST'])
@auth.login_required
@save_request
def create_joke():
	"""Создает и добавляет шутку с полученным 
	текстом к текущему пользователю
	"""
	data = request.get_json() or {}
	try:
		joke = joke_schema.load(data)
	except ValidationError as e:
		return make_response(({'errors': e.messages}, 400))
	joke.user = g.current_user
	joke.save()
	return make_response((joke_schema.jsonify(joke), 201))


@app.route('/jokes/<int:id>', methods=['PUT'])
@auth.login_required
@save_request
def update_my_joke(id):
	"""Изменяет информацию о выбранной шутке, 
	если она принадлежит текущему пользователю
	"""
	joke = Joke.query.filter_by(id=id).one_or_none()
	error = get_potential_errors(joke)
	if error:
		return error
	data = request.get_json() or {}
	try:
		joke = joke_schema.load(data, instance=joke)
	except ValidationError as e:
		response = make_response(({'errors': e.messages}, 400))
		return response
	joke.save()
	return joke_schema.jsonify(joke)


@app.route('/jokes/<int:id>', methods=['DELETE'])
@auth.login_required
@save_request
def delete_my_joke(id):
	"""Удаляет выбранную шутку, 
	если она принадлежит текущему пользователю
	"""
	joke = Joke.query.filter_by(id=id).one_or_none()
	error = get_potential_errors(joke)
	if error:
		return error
	joke.delete()
	return make_response(('', 204))