#!usr/bin/python
import unittest
import os
import json
from jokes import app, db
from requests.auth import _basic_auth_str
from config import basedir
from jokes.models import User, Joke


class JokesTastCase(unittest.TestCase):
	""" Список тестов:
	get_my_jokes - функция 'get_my_jokes' с правильными данными
	get_my_jokes_without_auth - функция 'get_my_jokes' без аутентификации
	invalid_auth - любая функция('get_my_jokes') с неправильными данными аутентификации,
		так как система аутентификации одна у всех функций этого типа
	generate_joke - функция 'generate_joke' с правильными данными
	generate_joke_without_auth - функция 'generate_joke' без аутентификации
	get_my_joke - функция 'get_my_joke' с правильными данными
	get_my_joke_without_auth - функция 'get_my_joke' без аутентификации
	get_not_my_joke - функция 'get_my_joke' с шуткой, не принадлежащей текущему пользователю
	get_not_exist_joke - функция 'get_my_joke' с идентификатором несуществующей шутки
	create_joke - функция 'create_joke' с правильными данными
	create_joke_without_auth - функция 'create_joke' без аутентификации
	create_joke_missing_text - функция 'create_joke' с отсутствующим значением поля 'text' в запросе
	create_joke_unknown_field - функция 'create_joke' с неизвестным параметром в запросе
	create_duplicate_joke - функция 'create_joke' с неуникальным текстом
	update_my_joke - функция 'update_my_joke' с правильными данными
	update_joke_without_auth - функция 'update_my_joke' без аутентификации
	update_not_my_joke - функция 'update_my_joke' с шуткой, не принадлежащей текущему пользователю
	update_not_exist_joke - функция 'update_my_joke' с идентификатором несуществующей шутки
	update_duplicate_joke - функция 'update_my_joke' с неуникальным текстом
	update_joke_missing_text - функция 'update_my_joke' с отсутствующим значением поля 'text' в запросе
	update_joke_unknown_field - функция 'update_my_joke' с неизвестным параметром в запросе
	delete_my_joke - функция 'delete_my_joke' с правильными данными
	delete_joke_without_auth - функция 'delete_my_joke' без аутентификации
	delete_not_my_joke - функция 'delete_my_joke' с шуткой, не принадлежащей текущему пользователю
	delete_not_exist_joke - функция 'delete_my_joke' с идентификатором несуществующей шутки
	"""
	def setUp(self):
		app.config['TESTING'] = True
		app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
			os.path.join(basedir, 'test.db')
		self.app = app.test_client()
		db.drop_all()
		db.create_all()

	def test_get_my_jokes(self):
		user = User(username='user1', password='pass1')
		user.jokes = [Joke(text='joke1'), Joke(text='joke2')]
		db.session.add(user)
		db.session.commit()
		user_jokes = [{'id': joke.id, 'text': joke.text} \
			for joke in user.jokes]
		result = self.app.get(
			'/jokes', 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})			
		self.assertEqual(result.status_code, 200)
		self.assertEqual(result.json, user_jokes)

	def test_get_my_jokes_without_auth(self):
		result = self.app.get('/jokes')			
		self.assertEqual(result.status_code, 401)

	def test_invalid_auth(self):
		user = User(username='user1', password='pass1')
		db.session.add(user)
		db.session.commit()
		result = self.app.get(
			'/jokes', 
			headers={'Authorization': _basic_auth_str(
				user.username, 'pass2')})
		self.assertEqual(result.status_code, 401)

	def test_generate_joke(self):
		user = User(username='user1', password='pass1')
		db.session.add(user)
		db.session.commit()
		result = self.app.get(
			'/jokes/generate', 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})			
		self.assertIn(result.status_code, [200, 503])
		if result.status_code == 200:
			self.assertIn(b'text', result.data)
		else:
			self.assertIn(
				b'Sorry, we cannot generate a joke for you', 
				result.data)

	def test_generate_joke_without_auth(self):
		result = self.app.get('/jokes/generate')			
		self.assertEqual(result.status_code, 401)

	def test_get_my_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		user_joke = {'id': joke.id, 'text': joke.text}
		result = self.app.get(
			'/jokes/{joke_id}'.format(joke_id=joke.id), 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 200)
		self.assertEqual(result.json, user_joke)		

	def test_get_my_joke_without_auth(self):
		result = self.app.get('/jokes/1')			
		self.assertEqual(result.status_code, 401)

	def test_get_not_my_joke(self):
		me = User(username='user1', password='pass1')
		somebody = User(username='user2', password='pass2')
		my_joke = Joke(text='joke1')
		sb_joke = Joke(text='joke2')
		me.jokes = [my_joke]
		somebody.jokes = [sb_joke]
		db.session.add(me)
		db.session.add(somebody)
		db.session.commit()
		result = self.app.get(
			'/jokes/{joke_id}'.format(joke_id=sb_joke.id), 
			headers={'Authorization': _basic_auth_str(
				me.username, me.password)})
		self.assertEqual(result.status_code, 403)

	def test_get_not_exist_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.get(
			'/jokes/{joke_id}'.format(joke_id=joke.id+1), 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 404)
		self.assertIn(b'Joke does not exist', result.data)

	def test_create_joke(self):
		user = User(username='user1', password='pass1')
		db.session.add(user)
		db.session.commit()
		result = self.app.post(
			'/jokes', json={'text': 'joke1'}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 201)
		
	def test_create_joke_without_auth(self):
		result = self.app.post('/jokes', json={'text': 'joke1'})			
		self.assertEqual(result.status_code, 401)

	def test_create_joke_missing_text(self):
		user = User(username='user1', password='pass1')
		db.session.add(user)
		db.session.commit()
		result = self.app.post(
			'/jokes', json={}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 400)
		self.assertIn(
			b'Missing data for required field.', result.data)
		self.assertIn(b'text', result.data)

	def test_create_joke_unknown_field(self):
		user = User(username='user1', password='pass1')
		db.session.add(user)
		db.session.commit()
		result = self.app.post(
			'/jokes', json={'text': 'joke1', 'key': 'value'}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 400)
		self.assertIn(b'Unknown field.', result.data)
		self.assertIn(b'key', result.data)

	def test_create_duplicate_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.post(
			'/jokes', json={'text': 'joke1'}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 400)
		self.assertIn(b'Your joke is not unique', result.data)

	def test_update_my_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.put(
			'/jokes/{joke_id}'.format(joke_id=joke.id), 
			json={'text': 'joke2'}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 200)

	def test_update_joke_without_auth(self):
		result = self.app.put('/jokes/1', json={'text': 'joke1'})			
		self.assertEqual(result.status_code, 401)

	def test_update_not_my_joke(self):
		me = User(username='user1', password='pass1')
		somebody = User(username='user2', password='pass2')
		my_joke = Joke(text='joke1')
		sb_joke = Joke(text='joke2')
		me.jokes = [my_joke]
		somebody.jokes = [sb_joke]
		db.session.add(me)
		db.session.add(somebody)
		db.session.commit()
		result = self.app.put(
			'/jokes/{joke_id}'.format(joke_id=sb_joke.id), 
			json={'text': 'joke3'}, 
			headers={'Authorization': _basic_auth_str(
				me.username, me.password)})
		self.assertEqual(result.status_code, 403)

	def test_update_not_exist_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.put(
			'/jokes/{joke_id}'.format(joke_id=joke.id+1), 
			json={'text': 'joke2'},
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 404)
		self.assertIn(b'Joke does not exist', result.data)

	def test_update_duplicate_joke(self):
		user = User(username='user1', password='pass1')
		user.jokes = [Joke(text='joke1'), Joke(text='joke2')]
		db.session.add(user)
		db.session.commit()
		result = self.app.put(
			'/jokes/{joke_id}'.format(joke_id=user.jokes[1].id), 
			json={'text': 'joke1'},
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 400)
		self.assertIn(b'Your joke is not unique', result.data)

	def test_update_joke_missing_text(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.put(
			'/jokes/{joke_id}'.format(joke_id=joke.id), json={}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 400)
		self.assertIn(
			b'Missing data for required field.', result.data)
		self.assertIn(b'text', result.data)

	def test_update_joke_unknown_field(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.put(
			'/jokes/{joke_id}'.format(joke_id=joke.id), 
			json={'key': 'value', 'text': 'joke2'}, 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 400)
		self.assertIn(b'Unknown field.', result.data)
		self.assertIn(b'key', result.data)

	def test_delete_my_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		joke_id = joke.id
		result = self.app.delete(
			'/jokes/{joke_id}'.format(joke_id=joke_id), 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 204)
		self.assertEqual(
			Joke.query.filter_by(id=joke_id).one_or_none(), None)

	def test_delete_joke_without_auth(self):
		result = self.app.delete('/jokes/1')			
		self.assertEqual(result.status_code, 401)

	def test_delete_not_my_joke(self):
		me = User(username='user1', password='pass1')
		somebody = User(username='user2', password='pass2')
		my_joke = Joke(text='joke1')
		sb_joke = Joke(text='joke2')
		me.jokes = [my_joke]
		somebody.jokes = [sb_joke]
		db.session.add(me)
		db.session.add(somebody)
		db.session.commit()
		result = self.app.delete(
			'/jokes/{joke_id}'.format(joke_id=sb_joke.id),
			headers={'Authorization': _basic_auth_str(
				me.username, me.password)})
		self.assertEqual(result.status_code, 403)

	def test_delete_not_exist_joke(self):
		user = User(username='user1', password='pass1')
		joke = Joke(text='joke1')
		user.jokes = [joke]
		db.session.add(user)
		db.session.commit()
		result = self.app.delete(
			'/jokes/{joke_id}'.format(joke_id=joke.id+1), 
			headers={'Authorization': _basic_auth_str(
				user.username, user.password)})
		self.assertEqual(result.status_code, 404)
		self.assertIn(b'Joke does not exist', result.data)

	def tearDown(self):
		db.session.remove()
		db.drop_all()


if __name__ == '__main__':
	unittest.main()