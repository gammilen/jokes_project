#!usr/bin/python
import os
from jokes.models import User, Joke
from jokes import db

"""Данные для создания экземпляров пользователей"""
USERS = [
	{'username': 'user1', 'password': 'pass1', 'jokes': [{'text': 'joke1'}]},
	{'username': 'user2', 'password': 'pass2', 'jokes': [{'text': 'joke2'}]},
	{'username': 'user3', 'password': 'pass3', 'jokes': [{'text': 'joke3'}]},
]

"""Отчистка базы и создание всех таблиц"""
db.drop_all()
db.create_all()

"""Создание экземпляров пользователей и их шуткой с созранением в базу"""
for user in USERS:
	u = User(username=user['username'], password=user['password'])
	for joke in user['jokes']:
		j = Joke(text=joke['text'])
		u.jokes.append(j)	
	db.session.add(u)
db.session.commit()