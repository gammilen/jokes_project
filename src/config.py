import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
	FLASK_APP = 'jokes'
	SECRET_KEY = os.environ.get('SECRET_KEY') or 'some-secret-key'
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
		'sqlite:///' + os.path.join(basedir, 'jokes.db')
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	TESTING = False
	DEBUG = False
